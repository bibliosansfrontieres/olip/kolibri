#!/bin/bash

. /opt/venv/bin/activate

set -e

export "PYTHONPATH"="${PYTHONPATH}:/root/.local/bin/"
export "KOLIBRI_HOME"="/data/"

# For offline devices like CMAL or Raspberry Pi use the OLIP network
# otherwise use the public dns name. Without this workaround we get a 500 error from OLIP
if [[ `uname -m` == "i686" || `uname -m` == "armv7l" ]]
then
    export "KOLIBRI_OIDC_CLIENT_URL"="http://173.17.0.1:5002"/oauth
else
    export "KOLIBRI_OIDC_CLIENT_URL"="$OIDC_URL"/oauth
fi

kolibri plugin enable kolibri_oidc_client_plugin
kolibri plugin enable kolibri_opensearch_plugin

exec "$@"
