FROM --platform=$TARGETPLATFORM offlineinternet/olip-base:latest as build

ENV DEBIAN_FRONTEND noninteractive

RUN apt update \
    && apt install -y python3-pip build-essential libssl-dev libffi-dev python3-dev cargo python3-venv
    
RUN python3 -m venv /opt/venv \
    && . /opt/venv/bin/activate \
    && /opt/venv/bin/python3 -m pip install --upgrade pip \
    && /opt/venv/bin/python3 -m pip install cryptography --no-binary cryptography

FROM --platform=$TARGETPLATFORM offlineinternet/olip-base:latest

RUN apt update \
    && apt install -y python3-pip libssl-dev libffi-dev

COPY --from=build /opt/ /opt/

WORKDIR /opt/

RUN echo "en_US.UTF-8 UTF-8" > /etc/locale.gen && \
    locale-gen en_US.UTF-8 && \
    dpkg-reconfigure locales && \
    /usr/sbin/update-locale LANG=en_US.UTF-8
ENV LC_ALL en_US.UTF-8

RUN . /opt/venv/bin/activate \
    && /opt/venv/bin/python3 -m pip install kolibri kolibri_opensearch_plugin kolibri_oidc_client_plugin

COPY entrypoint.sh /

ADD supervisor/* /etc/supervisor/conf.d/
RUN ln -sf /run/supervisord.log /var/log/supervisor/supervisord.log

ADD scripts/* /usr/local/bin/

EXPOSE 8080

ENTRYPOINT ["/entrypoint.sh"]

CMD ["/usr/bin/supervisord"]